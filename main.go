package main

import (
	"fmt"
	"log"

	"github.com/melbahja/goph"
	"golang.org/x/crypto/ssh"
)

func main() {
	auth := goph.Password("Integrador1")

	client, err := goph.NewConn(&goph.Config{
		User:     "u197816255",
		Addr:     "213.190.7.184",
		Auth:     auth,
		Port:     65002,
		Callback: ssh.InsecureIgnoreHostKey(),
	})
	if err != nil {
		log.Fatal(err)
	}

	// Defer closing the network connection.
	defer client.Close()

	sftp, err := client.NewSftp()

	if err != nil {
		fmt.Println(err)
	}

	//create file on server
	// file, err := sftp.Create("./domains/central-mx.com/public_html/gotest.txt")
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// file.Write([]byte(`Hello world`))
	// file.Close()

	//Search and find with linux commands
	// // Execute your command.
	// out, err := client.Run("find . -name Utils.log.2022-06-15")

	// if err != nil {
	// 	log.Fatal(err)
	// }

	// // Get your output as []byte.
	// fmt.Println(string(out))
	// lectura, err := client.Run("tail -20 " + string(out))
	// fmt.Println(string(lectura))

	// err = client.Download(string(out), "./logs/log.txt")
	// if err != nil {
	// 	fmt.Println(err)
	// }
}
